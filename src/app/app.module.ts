import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NumbersCardComponent } from './numbers-card/numbers-card.component';
import { GalleryComponent } from './gallery/gallery.component';
import { GalleryCardComponent } from './gallery-card/gallery-card.component';
import {FormsModule} from "@angular/forms";
import { CardComponent } from './card/card.component';


@NgModule({
  declarations: [
    AppComponent,
    NumbersCardComponent,
    GalleryComponent,
    GalleryCardComponent,
    CardComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
