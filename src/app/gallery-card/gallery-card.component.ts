import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-gallery-card',
  templateUrl: './gallery-card.component.html',
  styleUrls: ['./gallery-card.component.css']
})
export class GalleryCardComponent {
  @Input() title = 'name';
  @Input() url = 'https://eitrawmaterials.eu/wp-content/uploads/2016/09/person-icon.png';
}
