import {Component} from '@angular/core';

@Component({
  selector: 'app-numbers-card',
  templateUrl: './numbers-card.component.html',
  styleUrls: ['./numbers-card.component.css']
})
export class NumbersCardComponent {
  showRandomNumbers = false;
  numbers: number[] = [];


  randomNumbers() {
    let value = Math.floor(Math.random() * 36) + 1;
    this.numbers.push(value);
  }

  generateNumbers(){
    this.numbers = [];
    for (let i = 0; i < 5; i++){
      this.numbers[i];
      this.randomNumbers();
    }
  }

  onAddNumber(event: Event){
    event.preventDefault();
    this.generateNumbers();
  }
}
