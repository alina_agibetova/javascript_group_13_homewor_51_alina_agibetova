import {Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  password = '';
  title = '';
  url = '';
  showForm = false;
  showCard = false;
  pic = [
    {title:'name', url:'https://avatarko.ru/img/kartinka/33/multfilm_lyagushka_32117.jpg'},
    {title:'name', url:'https://avatarko.ru/img/kartinka/33/multfilm_lyagushka_32117.jpg'},
    {title:'name', url:'https://avatarko.ru/img/kartinka/33/multfilm_lyagushka_32117.jpg'},
    {title:'name', url:'https://avatarko.ru/img/kartinka/33/multfilm_lyagushka_32117.jpg'},
    {title:'name', url:'https://avatarko.ru/img/kartinka/33/multfilm_lyagushka_32117.jpg'},
  ];



  onAddGallery(event: Event){
    event.preventDefault();
    this.pic.push({
      title: this.title,
      url: this.url
    });
  }

  formIsEmpty() {
    return this.password === '' || this.url === '';
  }

}
